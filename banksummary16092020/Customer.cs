﻿namespace banksummary16092020
{
    public class Customer
    {
        private static int numberOfCust;
        private readonly int _customerID;
        private readonly int _customerNumber;
        public string Name { get; private set; }
        public string PhNumber { get; private set; }

        public int CustomerID
        {
            get
            {
                return _customerID;
            }
        }

        public int CustomerNumber
        {
            get
            {
                return _customerID;
            }
        }

        public Customer(int id, string name, string phone)
        {
            numberOfCust += 1;
            _customerNumber = numberOfCust;
            _customerID = id;
            Name = name;
            PhNumber = phone;
        }


        public static bool operator ==(Customer a, Customer b)
        {
            return !(a is null) &&
                   !(b is null) &&
                   a.CustomerNumber == b.CustomerNumber;
        }

        public static bool operator !=(Customer a, Customer b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            return this == obj as Customer;
        }

        public override int GetHashCode()
        {
            return this.CustomerNumber;
        }
        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(PhNumber)}: {PhNumber}, {nameof(CustomerID)}: {CustomerID}, {nameof(CustomerNumber)}: {CustomerNumber}";
        }
    }
}