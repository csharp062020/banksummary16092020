﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace banksummary16092020
{
    [Serializable]
    internal class AccountIsNullException : Exception
    {
        public AccountIsNullException()
        {
        }

        public AccountIsNullException(string message) : base(message)
        {
        }

        public AccountIsNullException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public AccountIsNullException(string message, List<Exception> innerException) : base(message)
        {
            throw new NotImplementedException();
        }

        protected AccountIsNullException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}