﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace banksummary16092020
{
    [Serializable]
    internal class CustomerIsNullException : Exception
    {
        public CustomerIsNullException()
        {
        }

        public CustomerIsNullException(string message) : base(message)
        {
        }

        public CustomerIsNullException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public CustomerIsNullException(string message, List<Exception> innerException) : base(message)
        {
            throw new NotImplementedException();
        }

        protected CustomerIsNullException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}