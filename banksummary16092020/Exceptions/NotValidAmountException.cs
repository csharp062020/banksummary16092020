﻿using System;
using System.Runtime.Serialization;

namespace banksummary16092020
{
    [Serializable]
    internal class NotValidAmountException : Exception
    {
        public NotValidAmountException()
        {
        }

        public NotValidAmountException(string message) : base(message)
        {
        }

        public NotValidAmountException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotValidAmountException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}