﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel.Design;

namespace banksummary16092020
{
    public class Bank : IBank
    {
        private List<Account> _accounts;
        private List<Customer> _customers;
        private Dictionary<int, Customer> _mapCustomersById;
        private Dictionary<int, Customer> _mapCustomersByNumber;
        private Dictionary<int, Account> _mapAccountByAccountNumber;
        private Dictionary<Customer, List<Account>> _mapAccountByCustomer;
        private int _totalMoneyInBank = 0;
        private int _profits = 0;

        public string Name { get; }
        public string Address { get; }
        public int CustomerCount { get; }

        public Bank()
        {
            _accounts = new List<Account>();
            _customers = new List<Customer>();
            _mapCustomersById = new Dictionary<int, Customer>();
            _mapCustomersByNumber = new Dictionary<int, Customer>();
            _mapAccountByAccountNumber = new Dictionary<int, Account>();
            _mapAccountByCustomer = new Dictionary<Customer, List<Account>>();
        }

        internal Customer GetCustomerByID(int customerId)
        {
            if (_mapCustomersById.TryGetValue(customerId, out Customer value))
            {
                return value;
            }
            else
            {
                throw new CustomerNotFoundException($"CustomerNotFoundException: try to get customer by id: {customerId}");
            }
        }
        internal Customer GetCustomerByNumber(int customerNumber)
        {
            if (_mapCustomersById.TryGetValue(customerNumber, out Customer value))
            {
                return value;
            }
            else
            {
                throw new CustomerNotFoundException($"CustomerNotFoundException: try to get customer by Number: {customerNumber}");
            }
        }
        internal Account GetAccountByNumber(int accountNumber)
        {
            if (_mapAccountByAccountNumber.TryGetValue(accountNumber, out Account value))
            {
                return value;
            }
            else
            {
                throw new AccountNotFoundException($"AccountNotFoundException: try to get Account by Number: { accountNumber}");
            }
        }
        internal List<Account> GetAccountsByCustomer(Customer customer)
        {
            if (_mapAccountByCustomer.TryGetValue(customer, out List<Account> value))
            {
                return value;
            }
            else
            {
                throw new CustomerNotFoundException($"CustomerNotFoundException: try to get customer by Number: {customer}");
            }
        }

        internal void AddNewCustomer(Customer customer)
        {
            List<Exception> exceptions = new List<Exception>();

            if (customer is null)
            {
                throw new CustomerIsNullException("the is no instance for new customer");
            }

            try
            {
                AddCustomerToCustomersList(customer);
            }
            catch (CustomerAlreadyExistException e)
            {
                Console.WriteLine($"{e.Message}");
                exceptions.Add(e);
            }

            try
            {
                AddCustomerToMapCustomersById(customer);
            }
            catch (CustomerAlreadyExistException e)
            {
                Console.WriteLine($"{e.Message}");
                exceptions.Add(e);
            }

            try
            {
                AddCustomerToMapCustomersByNumber(customer);
            }
            catch (CustomerAlreadyExistException e)
            {
                Console.WriteLine($"{e.Message}");
                exceptions.Add(e);
            }

            if (exceptions.Count > 0)
            {
                throw new CustomerIsNullException("exceptions on add customer");
            }

        }

        internal void OpenNewAccount(Account account, Customer customer)
        {
            List<Exception> exceptions = new List<Exception>();
            try
            {
                if (AccountDoesNotExist(account))
                {
                    _accounts.Add(account);
                }
            }
            catch (AccountAlreadyExistException e)
            {
                Console.WriteLine($"{e.Message}");
                exceptions.Add(e);
            }

            try
            {
                AddAccountToMapAccountByAccountNumber(account);
            }
            catch (AccountAlreadyExistException e)
            {
                Console.WriteLine($"{e.Message}");
                exceptions.Add(e);
            }
            try
            {
                AddCustomerToMapAccountByCustomer(account);
            }
            catch (AccountAlreadyExistException e)
            {
                Console.WriteLine($"{e.Message}");
                exceptions.Add(e);
            }
        }

        internal int Deposit(Account account, int amount)
        {
            try
            {
                account.SabTrack(amount);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                amount = account.Balance - account.MaxMinusAllowed;
                account.SabTrack(amount);
            }

            _totalMoneyInBank -= amount;
            return amount;
        }

        internal int Withdraw(Account account, int amount)
        {
            try
            {
                account.Add(amount);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            _totalMoneyInBank += amount;
            return amount;
        }

        internal int GetCustomerTotalBalance(Customer customer)
        {
            List<Account> customerAccounts;
            int totlaBalance = 0;
            try
            {
                if (_mapAccountByCustomer.TryGetValue(customer,out customerAccounts))
                {
                    foreach (Account account in customerAccounts)
                    {
                        totlaBalance += account.Balance;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 0;
            }

            return totlaBalance;
        }

        internal void CloseAccount(Account account, Customer customer)
        {
            if (account is null || customer is null)
            {
                if (customer is null)
                {
                    throw new CustomerIsNullException("cant close the account customer is null");
                }

                if (account is null)
                {
                    throw new AccountIsNullException("cant close the account, account is null");

                }
            }
            if (account.AccountOwner != customer)
            {
                throw new NotSameCustomerException("the account owner is not the same , to the customer you provide ");
            }
            if (account.Balance<0)
            {
                throw new BalanceException($"Can't close your Account until you cover your debit {account}");
            }
            if (account.Balance > 0)
            {
                throw new BalanceException($"Can't close your Account until you take your cash {account}");
            }

            try
            {
                RemoveAccount(account);
            }
            catch (Exception e)
            {
                Console.WriteLine($"problem to delete account {account} , {customer}",e);
            }
        }

        internal void ChargeAnnualCommossion(float percemtage)
        {
            int amount = 0;
            int balance = 0;

            float percemtageOnMinus = percemtage * 2;
            foreach (Account account in _accounts)
            {
                balance = account.Balance;
                amount = 0;
                if (balance<0)
                {
                    amount -= (int)(balance * percemtageOnMinus/100);
                }
                else
                {
                    amount = (int)(balance * percemtage / 100);
                }

                account.SabTrack(amount,true);
                _profits += amount;
            }
        }

        internal void JoinAccounts(Account a, Account b)
        {
            try
            {
                Account c = a + b;
                RemoveAccount(a);
                RemoveAccount(b);
                OpenNewAccount(c,c.AccountOwner);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        private bool RemoveAccount(Account account)
        {
            _accounts.Remove(account);
            _mapAccountByAccountNumber.Remove(account.AccountNumber);
            _mapAccountByCustomer.TryGetValue(account.AccountOwner, out List<Account> accountsList); 
            accountsList.Remove(account);

            return true;
        }
        private bool CustomerDoesNotExist(Customer customer, bool fromList = false)  
        {

            return ((!fromList &&
                      !_mapCustomersById.ContainsKey(customer.CustomerID) &&
                      !_mapCustomersByNumber.ContainsKey(customer.CustomerNumber)) ||
                     !_customers.Contains(customer));
        }

        private bool AccountDoesNotExist(Account account, bool fromList = false) 
        {
            return (!fromList &&
                     !_mapAccountByAccountNumber.ContainsKey(account.AccountNumber) &&
                     !_mapAccountByCustomer.ContainsKey(account.AccountOwner) &&
                     !_mapAccountByCustomer[account.AccountOwner].Contains(account)) ||
                   !_accounts.Contains(account);
        }

        private void AddCustomerToCustomersList(Customer customer)
        {
            if (CustomerDoesNotExist(customer))
            {
                _customers.Add(customer);
            }
            else
            {
                throw new CustomerAlreadyExistException("Customer Not Added to Id dictionary");
            }
        }

        private void AddCustomerToMapCustomersById(Customer customer)
        {
            if (!_mapCustomersById.ContainsKey(customer.CustomerID))
            {
                _mapCustomersById.Add(customer.CustomerID, customer);
            }
            else
            {
                throw new CustomerAlreadyExistException("Customer Not Added to Id dictionary");
            }
        }
        private void AddCustomerToMapCustomersByNumber(Customer customer)
        {
            if (!_mapCustomersByNumber.ContainsKey(customer.CustomerNumber))
            {
                _mapCustomersByNumber.Add(customer.CustomerNumber, customer);
            }
            else
            {
                throw new AccountAlreadyExistException("Customer Not Added to Customer Number dictionary");
            }
        }

        private void AddAccountToMapAccountByAccountNumber(Account account)
        {
            if (!_mapAccountByAccountNumber.ContainsKey(account.AccountNumber))
            {
                _mapAccountByAccountNumber.Add(account.AccountNumber, account);
            }
            else
            {
                throw new AccountAlreadyExistException("Customer Not Added to Customer Number dictionary");
            }
        }

        private void AddCustomerToMapAccountByCustomer(Account account)
        {
            if (!_mapAccountByCustomer.ContainsKey(account.AccountOwner))
            {
                _mapAccountByCustomer.Add(account.AccountOwner, new List<Account>() { account });
            }
            else if (!_mapAccountByCustomer[account.AccountOwner].Contains(account))
            {
                _mapAccountByCustomer[account.AccountOwner].Add(account);
            }
            else
            {
                throw new AccountAlreadyExistException("The Account is already exist on list of accounts by Customer");
            }

        }

    }
}