﻿using System;

namespace banksummary16092020
{
    public class Account
    {
        private static int _numberOfAcc;
        private readonly int _accountNumber;
        private readonly Customer _accountOwner;
        private int _maxMinusAllowed;

        //dateCreaDateTime added to decide the max overdraft allow when join accounts.   
        private DateTime dateCreatDateTime;

        public int AccountNumber
        {
            get
            {
                return _accountNumber;
            }
        }

        public int Balance { get; private set; }

        public Customer AccountOwner
        {
            get
            {
                return _accountOwner;
            }
        }

        public int MaxMinusAllowed
        {
            get
            {
                return _maxMinusAllowed;
            }
            private set
            {
                _maxMinusAllowed = IncomeToOverDraftAndOvreDraftToIncome(value, true);
            }
        }

        public Account(Customer customer, int monthlyIncome)
        {
            _accountNumber = _numberOfAcc++;
            MaxMinusAllowed = monthlyIncome;
            dateCreatDateTime = DateTime.Now;
        }

        private int Income(int maxOverdraftAllow)
        {
            return IncomeToOverDraftAndOvreDraftToIncome(maxOverdraftAllow, false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"> </param>
        /// <param name="overdraft"> true for Get OverDraft (with minus) - false to get the Monthly income </param>
        /// <returns></returns>
        private int IncomeToOverDraftAndOvreDraftToIncome(int x, bool overdraft = true)
        {
            int howManyToDouble = -3;
            return overdraft ? x * howManyToDouble : Math.Abs(x) / Math.Abs(howManyToDouble);
        }

        public void Add(int amount)
        {
            if (amount <=0 )
            {
                Balance += amount;
            }
            else
            {
                throw new BalanceException($"you can't add a negative number - use sabtrak . account {this}");
            }
        }

        public void SabTrack(int amount , bool profit = false)
        {
            if ( profit || AllowToSubTrack(amount) )
            {
                Balance -= amount;
            }
            else
            {
                throw new BalanceException($"sorry you can't get this amount:{amount} maximum allows {Balance - MaxMinusAllowed}\nyour balance is {Balance} , max Overdraft {MaxMinusAllowed}");
            }
        }

        private bool AllowToSubTrack(int amount)
        {
            return Balance - amount >= MaxMinusAllowed;
        }

        public override string ToString()
        {
            return $"Open Date: {dateCreatDateTime}\n{nameof(AccountNumber)}: {AccountNumber}\n{nameof(AccountOwner)}: {AccountOwner}\n{nameof(Balance)}: {Balance}\n{nameof(MaxMinusAllowed)}: {MaxMinusAllowed}";
        }

        public static bool operator ==(Account a, Account b)
        {
            if (a is null || b is null)
            {
                return false;
            }
            return a.AccountNumber == b.AccountNumber;
        }

        public static bool operator !=(Account a, Account b)
        {
            return !(a == b);
        }


        public override bool Equals(object obj)
        {
            return this == obj as Account;
        }

        public override int GetHashCode()
        {
            return this.AccountNumber;
        }

        public static Account operator +(Account a, Account b)
        {
            try
            {
                ExceptionsForAddOperator(a, b);
            }
            catch (AccountNotFoundException e)
            {
                Console.WriteLine("One or more account not found so the customer is not the same , No changes were made!!");
                throw new NotSameCustomerException("One or more account not found so the customer is not the same , No changes were made!!", e);
            }
            catch (NotSameCustomerException e)
            {
                Console.WriteLine("the owners of the accounts is different ,No changes were made");
                throw new NotSameCustomerException("the owners of the accounts is different ,No changes were made", e);
            }

            return a.dateCreatDateTime > b.dateCreatDateTime ? CombineAccount(a, b) : CombineAccount(b, a);
        }

        public static int operator +(Account a, int positiveAmount)
        {
            if (a is null)
            {
                throw new AccountNotFoundException("AccountNotFoundException: Account A is null");
            }

            if (positiveAmount < 0)
            {
                throw new NotValidAmountException($"Can't add A minus number please use SubTrackFunction account: {a}, amount AddTry {positiveAmount}");
            }

            return a.Balance += positiveAmount;
        }

        public static int operator -(Account a, int positiveAmount)
        {
            if (a is null)
            {
                throw new AccountNotFoundException("AccountNotFoundException: Account A is null");
            }

            if (positiveAmount < 0)
            {
                throw new NotValidAmountException($"Can't Sub A minus number please use AddTrack account: {a}, amount AddTry {positiveAmount}");
            }

            a.SabTrack(positiveAmount);
            return a.Balance;
        }


        private static void ExceptionsForAddOperator(Account a, Account b)
        {
            AccountIsNullExceptionMetod(a, b);

            if (a == b)
            {
                throw new AccountAlreadyExistException($"AccountAlreadyExistException: can't do the + operator it is the same account\n{a}\n{b}");
            }

            if (a.AccountOwner != b.AccountOwner)
            {
                throw new NotSameCustomerException($"NotSameCustomerException: can't do the + operator\nA: {a.AccountOwner.ToString()}\nB: {b.AccountOwner.ToString()}");
            }
        }

        private static void AccountIsNullExceptionMetod(Account a)
        {
            if (a is null)
            {
                throw new AccountIsNullException("CustomerIsNullException: account is null");
            }
        }

        private static void AccountIsNullExceptionMetod(Account a, Account b)
        {
            if (a is null || b is null)
            {
                if (a is null && b is null)
                    throw new AccountIsNullException("CustomerIsNullException: 2 accounts are null");
                else if (a is null)
                    throw new AccountIsNullException("CustomerIsNullException: Account A is null");
                else if (b is null)
                    throw new AccountIsNullException("CustomerIsNullException: Account B is null");
            }
        }

        private static Account CombineAccount(Account newestAccount, Account olderAccount)
        {
            Account newAccount = new Account(newestAccount.AccountOwner, newestAccount.Income(newestAccount.MaxMinusAllowed));
            newAccount.Balance = newestAccount.Balance + olderAccount.Balance;
            return newAccount;
        }

    }
}